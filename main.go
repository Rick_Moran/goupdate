package main

import (
	"flag"
	"os"
	"runtime"

	"bitbucket.org/krakencode/goupdate/packages/gocheck"
	"bitbucket.org/krakencode/goupdate/packages/goinstall"
	"github.com/kardianos/osext"
)

// VersionNum holds the current version of goupdate
const VersionNum = "1.0.3"

var (
	check4Update  bool
	installUpdate bool
	sysOs         string
	sysArch       string
	appPath       string
)

func init() {
	appPath, _ = osext.ExecutableFolder()
	flag.BoolVar(&check4Update, "check", false, "Force the version check")
	flag.BoolVar(&installUpdate, "install", false, "Pass to install the update")
	flag.Parse()

	// Get OS Info
	sysOs = runtime.GOOS
	sysArch = runtime.GOARCH
}

func main() {

	switch {
	default:
		myChk := gocheck.GoCheck{SystemOS: sysOs, SysArch: sysArch, Path: appPath, ForceCheck: check4Update, VersionNum: VersionNum}
		myChk.Run()

	case installUpdate:
		myInst := goinstall.GoInstall{SystemOS: sysOs, SysArch: sysArch, VersionNum: VersionNum}
		myInst.Run()

	}

	os.Exit(0)
	/*






		isNew := versionCheck(sysVersion, curVersion)

		if isNew {
			myCol := msg.Red.SprintFunc()
			fmt.Println(myCol("[Need Update]"))

			colInform := msg.Magenta.PrintlnFunc()

			fileTar := fmt.Sprintf("go%s.%s-%s.tar.gz", curVersion, sysOs, sysArch)

			cmdCurl := fmt.Sprintf("sudo curl https://storage.googleapis.com/golang/%s --output /tmp/%s", fileTar, fileTar)

			cmdUnTar := fmt.Sprintf("sudo tar -C /usr/local -xzf /tmp/%s", fileTar)

			// Display Msg
			colInform("Run to Update: ")
			fmt.Println(cmdCurl)
			fmt.Println("sudo rm -rf /usr/local/go")
			fmt.Println(cmdUnTar)
			fmt.Println("")

		} else {
			myCol := msg.Green.SprintFunc()
			fmt.Println(myCol("[Uptodate]"))
		}

		// Update File with todays date
		ioutil.WriteFile(fmt.Sprint(appPath, string(os.PathSeparator), "goupdate.dat"), []byte{}, 0600)
	*/
}
