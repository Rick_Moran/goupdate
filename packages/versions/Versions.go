package versions

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

// VersionCheck compares two version strings
func VersionCheck(a, b string) bool {
	if a == b {
		return false
	}

	var ret int
	as := strings.Split(a, ".")
	bs := strings.Split(b, ".")
	loopMax := len(bs)
	if len(as) > len(bs) {
		loopMax = len(as)
	}
	for i := 0; i < loopMax; i++ {
		var x, y string
		if len(as) > i {
			x = as[i]
		}
		if len(bs) > i {
			y = bs[i]
		}
		xi, _ := strconv.Atoi(x)
		yi, _ := strconv.Atoi(y)
		if xi > yi {
			ret = -1
		} else if xi < yi {
			ret = 1
		}
		if ret != 0 {
			break
		}
	}

	if ret < 0 {
		return false
	}
	return true
}

// GetVersionApp will return the current version installed on the machine
func GetVersionApp() (string, error) {

	out, err := exec.Command("go", "version").Output()
	if err != nil {
		return "", err
	}

	tmp := strings.Split(string(out), "version go")
	if len(tmp) > 1 {
		tmp = strings.Split(tmp[1], " ")
		return tmp[0], nil
	}
	return "", fmt.Errorf("The Output was not what was expected")
}

// GetVersionWeb will return the most current version listed on the web
func GetVersionWeb() (string, error) {
	res, err := http.Get("https://golang.org/dl/")
	if err != nil {
		log.Fatal(err)
	}

	th := html.NewTokenizer(res.Body)
	for {
		tokenType := th.Next()
		curToken := th.Token()

		switch {
		case tokenType == html.StartTagToken:
			// Check div for Ids
			if curToken.Data == "div" {
				for _, a := range curToken.Attr {
					if a.Key == "id" && strings.HasPrefix(a.Val, "go") {
						return strings.TrimPrefix(a.Val, "go"), nil
					}
				}
			}
		case tokenType == html.EndTagToken:
			if curToken.Data == "html" {
				return "", fmt.Errorf("No Tags Found on site")
			}
		}
	}
}
