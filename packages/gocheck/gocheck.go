package gocheck

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"

	msg "bitbucket.org/krakencode/goupdate/packages/messages"
	ver "bitbucket.org/krakencode/goupdate/packages/versions"
)

// GoCheck will hold all the data needed for a system check
type GoCheck struct {
	ForceCheck bool
	SystemOS   string
	SysArch    string
	Path       string
	VersionNum string
}

// Run will check on version and display as needed
func (g *GoCheck) Run() {
	// If Already Checked today Leave
	if g.IsCheckedToday() && !g.ForceCheck {
		return
	}

	// Set Version

	// Title
	msg.Sprint(msg.Cyan, "[GoUpdater]")

	// App Version
	msg.Sprint(msg.Magenta, fmt.Sprintf("[%s]", g.VersionNum))

	// Get System Version
	sysVersion, err := ver.GetVersionApp()
	if err != nil {
		fmt.Println("")
		msg.Sprint(msg.Red, err.Error())
	}
	fmt.Printf("[System: %s]", sysVersion)

	// Get Web Version
	curVersion, err := ver.GetVersionWeb()
	if err != nil {
		fmt.Println("")
		msg.Sprint(msg.Red, err.Error())
	}
	msg.Sprint(msg.Yellow, fmt.Sprintf("[Current: %s]", curVersion))

	isNew := ver.VersionCheck(sysVersion, curVersion)

	if isNew {
		msg.Sprint(msg.Red, "[Need Update]")
	} else {
		msg.Sprint(msg.Green, "[Uptodate]")
	}
	fmt.Println("")

	// Update File with todays date
	ioutil.WriteFile(fmt.Sprint(g.Path, string(os.PathSeparator), "goupdate.dat"), []byte{}, 0600)
}

// IsCheckedToday will check the date of the goupdate.dat file and if not today will check on update
func (g *GoCheck) IsCheckedToday() bool {
	// Get File Info
	fInfo, err := os.Stat(fmt.Sprint(g.Path, string(os.PathSeparator), "goupdate.dat"))
	if err != nil {
		ioutil.WriteFile(fmt.Sprint(g.Path, string(os.PathSeparator), "goupdate.dat"), []byte{}, 0600)
		return false
	}
	modTime := fInfo.ModTime()
	fYear, fMonth, fDay := modTime.Date()

	curTime := time.Now()
	cYear, cMonth, cDay := curTime.Date()

	if cYear <= fYear && cMonth <= fMonth && cDay <= fDay {
		return true
	}
	return false
}
