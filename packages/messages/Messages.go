package messages

import (
	"fmt"

	"github.com/fatih/color"
)

// App Info
const (
	AppName = "GoUpdate"
)

// Available Colors fot print in
var (
	Green   = *color.New(color.FgGreen)
	Cyan    = *color.New(color.FgCyan)
	Magenta = *color.New(color.FgMagenta)
	Yellow  = *color.New(color.FgYellow)
	Red     = *color.New(color.FgRed)
)

// Sprint will print the text in the color of choosing
func Sprint(color color.Color, msg string) {
	myCol := color.SprintFunc()
	fmt.Print(myCol(msg))
}
