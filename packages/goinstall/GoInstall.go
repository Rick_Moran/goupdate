package goinstall

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"strings"

	msg "bitbucket.org/krakencode/goupdate/packages/messages"
	ver "bitbucket.org/krakencode/goupdate/packages/versions"
)

// GoInstall holds all for the Install
type GoInstall struct {
	curUser    *user.User
	SystemOS   string
	SysArch    string
	VersionNum string
}

// Run will do the install
func (g *GoInstall) Run() {
	var err error

	// Get Current user .. and verify root
	g.curUser, err = user.Current()
	if err != nil || g.curUser.Username != "root" {
		msg.Sprint(msg.Red, "Install must be ran as root ( sudo )\n")
		os.Exit(1)
	}

	// Current Version
	curVer, err := ver.GetVersionWeb()
	if err != nil {
		msg.Sprint(msg.Red, "Unable to get lattest version\n")
		os.Exit(1)
	}

	// Create Dest folder
	destPath := fmt.Sprintf("/usr/local/go.%s", curVer)

	// Check if we already have it
	_, err = os.Stat(destPath)
	if err == nil {
		msg.Sprint(msg.Red, "Version already installed\n")
		os.Exit(1)
	}

	// Generate DL Url
	url := fmt.Sprintf("https://storage.googleapis.com/golang/go%s.%s-%s.tar.gz", curVer, g.SystemOS, g.SysArch)

	// Create TMP folder
	dlDir, err := ioutil.TempDir("", "GoUpdate.")
	if err != nil {
		msg.Sprint(msg.Red, "Unable to create TMP folder\n")
		os.Exit(1)
	}

	fmt.Println(url)
	fmt.Println(dlDir)

	// Download File

	fmt.Println("Downloading the tar.gz file for golang. This might take a few minutes")
	msg.Sprint(msg.Magenta, "Download golang .......")
	err = dlFile(url, fmt.Sprint(dlDir, "/golang.tar.gz"))
	if err != nil {
		msg.Sprint(msg.Red, "[Fail]\n")
		msg.Sprint(msg.Red, "Error Downloading Golang\n")
		os.Exit(1)
	}
	msg.Sprint(msg.Green, "[Success]\n")

	// Extract File to Dest
	err = extractTarGz(fmt.Sprint(dlDir, "/golang.tar.gz"), destPath)
	if err != nil {
		msg.Sprint(msg.Red, fmt.Sprint("Unable to extract Files", err, "\n"))
		os.Exit(1)
	}

	// Delete Symlink
	err = g.deleteGoFolder()
	if err != nil {
		msg.Sprint(msg.Red, "Unable to cleanup symlink in /usr/local \n")
		os.Exit(1)
	}

	// Make new Symlink
	os.Symlink(destPath, "/usr/local/go")
}

func (g *GoInstall) deleteGoFolder() error {
	file, err := os.Lstat("/usr/local/go")
	if err != nil {
		return err
	}
	if file.Mode()&os.ModeSymlink == os.ModeSymlink {
		return os.Remove("/usr/local/go")
	}
	return os.RemoveAll("/usr/local/go")
}

func extractTarGz(inPath, outPath string) error {

	// Tar.Gz
	file, err := os.Open(inPath)
	if err != nil {
		return err
	}
	defer file.Close()

	// Read GZ
	fileReader, err := gzip.NewReader(file)
	if err != nil {
		return err
	}

	tarReader := tar.NewReader(fileReader)

	for {
		header, err := tarReader.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		// get the individual filename and extract to the current directory
		filename := fmt.Sprint(outPath, string(os.PathSeparator), strings.TrimPrefix(header.Name, "go/"))

		switch header.Typeflag {
		case tar.TypeDir:
			// handle directory
			fmt.Println("Creating directory :", filename)
			err = os.MkdirAll(filename, os.FileMode(header.Mode))
			if err != nil {
				return err
			}

		case tar.TypeReg:
			// handle normal file
			fmt.Println("Untarring :", filename)

			writer, err := os.Create(filename)
			if err != nil {
				return err
			}

			io.Copy(writer, tarReader)
			err = os.Chmod(filename, os.FileMode(header.Mode))
			if err != nil {
				return err
			}

			writer.Close()
		default:
			fmt.Printf("Unable to untar type : %c in file %s", header.Typeflag, filename)
		}
	}

	return nil
}

func dlFile(url, path string) error {
	// don't worry about errors
	response, err := http.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	//open a file for writing
	file, err := os.Create(path)
	if err != nil {
		return err
	}

	// Use io.Copy to just dump the response body to the file. This supports huge files
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	file.Close()
	return nil
}
